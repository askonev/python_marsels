from dataclasses import dataclass, astuple


@dataclass
class Point:

	x: float
	y: float
	z: float

	def __add__(first, second):
		if not isinstance(second, Point):
			return NotImplemented
		x1, y1, z1 = first
		x2, y2, z2 = second
		return Point(
			x1 + x2,
			y1 + y2,
			z1 + z2)

	def __sub__(first, second):
		if not isinstance(second, Point):
			return NotImplemented
		x1, y1, z1 = first
		x2, y2, z2 = second
		return Point(
			x1 - x2,
			y1 - y2,
			z1 - z2)

	def __mul__(self, number):
		if not isinstance(number, (int, float)):
			return NotImplemented
		x, y, z = self
		return Point(
			x * number,
			y * number,
			z * number)

	__rmul__ = __mul__

	def __iter__(self):
		yield from astuple(self)


if __name__ == '__main__':
	p1 = Point(1, 2, 3)
	p2 = p1 * 2
	print(p2)
	p3 = 3 * p1
	print(p3)
