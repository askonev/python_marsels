from collections import Counter
import re


def count_words(text) -> dict:
    return Counter(re.findall(r"\b[\w'-]+\b", text.lower()))


if __name__ == '__main__':
    print(count_words("¿Te't gust-a Python?"))
