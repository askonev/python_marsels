from typing import Iterable
from collections import deque


def window(
    data: Iterable,
    count: int,
    *,
    fillvalue = None):

    if count == 0:
        return None

    it = iter(data)
    buf = deque(maxlen=count)
    for _ in range(count):
        buf.append(next(it, fillvalue))
    yield tuple(buf)

    for item in it:
        buf.append(item)
        yield tuple(buf)


if __name__ == "__main__":
    print(list(window([], 0)))
