import math


DEFAULT = None
DEFAULT_STEP = 1
DEFAULT_START = 0


class float_range:
    def __init__(self, /, start: float, stop=DEFAULT, step=DEFAULT):
        if stop is DEFAULT:
            stop = start
            start = DEFAULT_START
            step = DEFAULT_STEP
        elif step is DEFAULT:
            step = DEFAULT_STEP

        self.start = start
        self.stop = stop
        self.step = step

    def isValidParameters(self):
        result = ((self.stop < self.start) and (self.step < 0))\
            or ((self.stop > self.start) and self.step > 0)
        return result

    def __iter__(self):
        current_ = self.start
        for i in range(len(self)):
            yield current_
            current_ += self.step

    def __len__(self):
        if not self.isValidParameters():
            return 0
        else:
            amplitude = self.stop - self.start
            return math.ceil(amplitude / self.step)

    def __repr__(self):
        return f"float_range({self.start}, {self.stop}, {self.step})"

    def __getitem__(self, key):
        if isinstance(key, int):
            if key >= len(self) or key < -len(self):
                raise IndexError
            if key < 0:
                key = len(self) + key
            return self.start + self.step * key
        elif isinstance(key, slice):
            istart, istop, istep = key.indices(len(self))
            if istart < istop:
                start = self[istart] if istart < len(self) else self[-1]
                stop = self[-1] + self.step / 2 if istop == len(self) else self[istop]
            else:
                return float_range(0, 0, 0)
            return float_range(start, stop, self.step * istep)

    def __eq__(self, other):
        if isinstance(other, (float_range, range)):
            len_other = len(other)
            len_self = len(self)
            is_equal = (len_other == 0 and len_self == 0)
            is_equal = is_equal or (self.start == other.start and (len_self, len_other) == (1, 1))
            is_equal = is_equal or (self.start, self[-1], self.step) == (other.start, other[-1], other.step)
            return is_equal
        else:
            return other == self


if __name__ == "__main__":
    r = float_range(0.5, 7, 0.75)
    print(len(r))
    print(list(r[len(r)::]), [])
