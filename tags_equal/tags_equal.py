

def is_equal(first: dict, second: dict):
    DEFAULT_ITEM = object()
    for fkey in first.keys():
        svalue = second.get(fkey.lower(), DEFAULT_ITEM)
        print(f'{svalue is DEFAULT_ITEM} {svalue if svalue is not DEFAULT_ITEM else "Empty"} == {first[fkey]}')
        if svalue is DEFAULT_ITEM or svalue != first[fkey]:
            return False
    return True


    for fitem, sitem in zip(first.items(), second.items()):
        if (fitem[0].lower() != sitem[0].lower()
            or fitem[1].lower() != sitem[1].lower()):
            return False
    return True


def parse(text: str):
    src = text.strip('<> ').split()
    res = dict()
    res['label'] = src[0].lower()
    for item in src[1:]:
        sub = item.split('=')
        if len(sub) > 1:
            res[sub[0].lower()] = sub[1].lower()
        else:
            res[sub[0].lower()] = 'Empty'
    return res


def tags_equal(first: str, second: str):
    return is_equal(parse(first), parse(second))

if __name__ == "__main__":
    equal = tags_equal(
        "<img src=cats.jpg height=40>",
        "<IMG SRC=cats.jpg height=40>")
    print(equal)

