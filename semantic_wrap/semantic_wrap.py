from re import split, sub, compile
from textwrap import dedent
from typing import Callable
import argparse
import sys


class Wrapper(object):
    END_SYMBOLS = (".", "!", "?")

    def __init__(self):
        self.current_state = None

    def run(self, text: str):
        def state_looking_quot_pair_as_end(self: Wrapper, symbol: str):
            if symbol == "\"":
                self.current_state = state_eating_white_spaces
            return symbol

        def state_looking_quot_pair(self: Wrapper, symbol: str):
            if symbol == "\"":
                self.current_state = state_regular
            elif symbol in self.END_SYMBOLS:
                self.current_state = state_looking_quot_pair_as_end
            return symbol

        def state_eating_white_spaces(self: Wrapper, symbol: str):
            if symbol == " ":
                return ""
            elif symbol == "\n":
                self.current_state = state_regular
                return symbol
            else:
                self.current_state = state_regular
                return "\n" + symbol

        def state_regular(self: Wrapper, symbol: str):
            if symbol == "\"":
                self.current_state = state_looking_quot_pair
            elif symbol in self.END_SYMBOLS:
                self.current_state = state_eating_white_spaces
            return symbol

        self.current_state = state_regular

        for c in text:
            yield self.current_state(self, c)


def semantic_wrap_old(text: str):
    return "".join(Wrapper().run(text))


def semantic_wrap(text: str):
    splitter = compile(r"([.!?]\"?)[ ]+")
    return splitter.sub(r"\1\n", text)


if __name__ == '__main__':
    # text1 = (
    #     "1. 2."
    #     "\n\n"
    #     "3.")
    # text2 = (
    #     "Whether I'm teaching new Pythonistas or long-time Python "
    #     "programmers, I frequently find that **Python programmers "
    #     "underutilize multiple assignment**."
    # )
    # text = dedent("""
    #     I prefer putting quotes "inside the period". But not everyone does.

    #     Some put "quotes outside punctuation." It's quite common actually.
    # """).lstrip()
    # print(semantic_wrap(text))
    parser = argparse.ArgumentParser()
    parser.add_argument("path_to_file", type=str)

    args = parser.parse_args()

    with open(args.path_to_file) as file:
        sys.stdout.write(semantic_wrap(file.read()))
