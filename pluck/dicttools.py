
DEFAULT_VALUE = object()

def pluck(data: dict, *categories, sep: str = ".", default=DEFAULT_VALUE):

	res = []
	for category in categories:
		keys = category.split(sep)

		part = data
		try:
			for key in keys:
				part = part[key]
		except KeyError:
			if default is DEFAULT_VALUE:
				raise
			else:
				part = default
		res.append(part)

	return tuple(res) if len(res) > 1 else res[0]


if __name__ == '__main__':
	d = {'a': {'b': 5, 'z': 20}, 'c': {'d': 3}, 'x': 40}
	print(pluck(d, 'a.b', 'c.e', 'c.d', 'x', default=None))
