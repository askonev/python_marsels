

def get_earliest(*dates):
    def unpack(date):
        m, d, y = date.split('/')
        return y, m, d

    return min(dates, key=unpack)
