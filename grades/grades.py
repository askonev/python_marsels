def round_(value: float) -> float:
	return float(int(value + 0.5))


def percent_to_grade(mark: float, *, suffix=False, round=False) -> str:
	SPAN = 10

	if round:
		mark = round_(mark)

	if mark == 100:
		return "A+" if suffix else "A"

	marks = {
		10: "A",
		9: "A",
		8: "B",
		7: "C",
		6: "D",
	}

	def get_suffix_(mark: float) -> str:
		rem = mark % SPAN
		PLUS_RANGE_LOW = 7
		EMPTY_RANGE_LOW = 3

		if rem >= PLUS_RANGE_LOW:
			suffix_symbol = "+"
		elif rem < PLUS_RANGE_LOW and rem >= EMPTY_RANGE_LOW:
			suffix_symbol = ""
		else:
			suffix_symbol = "-"
		return suffix_symbol

	def stub(mark: float) -> str:
		return ""

	get_suffix = get_suffix_ if suffix else stub

	try:
		symbol = marks[mark // 10] + get_suffix(mark)
	except KeyError:
		symbol = "F"

	return symbol


def calculate_gpa(grades: list) -> float:

	to_digit = {
		"A+": 4.33,
		"A": 4.00,
		"A-": 3.67,
		"B+": 3.33,
		"B": 3.00,
		"B-": 2.67,
		"C+": 2.33,
		"C": 2.00,
		"C-": 1.67,
		"D+": 1.33,
		"D": 1.00,
		"D-": 0.67,
		"F": 0.00,
	}

	sum = 0.0
	for grade in grades:
		sum += to_digit.get(grade, 0.0)
	sum /= len(grades)

	return sum


if __name__ == '__main__':
	print(calculate_gpa(['A-', 'B+', 'C-', 'D+', 'F']))
