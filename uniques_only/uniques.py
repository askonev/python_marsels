from typing import Iterable
from collections.abc import Hashable
import cProfile


def uniques_only(values_list: Iterable) -> Iterable:

    current = next(iter(values_list))
    if(isinstance(current, Hashable)):
        init = dict

        def add_item(d: Hashable, item: Iterable):
            d[item] = 0
    else:
        init = list

        def add_item(li, item: Iterable):
            li.append(item)

    ulist = init()
    add_item(ulist, current)
    yield current

    for item in values_list:
        if item not in ulist:
            add_item(ulist, item)
            yield item


if __name__ == '__main__':
    hashables = [(n, n + 1) for n in range(1000)] + [0]
    unhashables = [[n] for n in range(1000)] + [0]
    cProfile.run("for _ in uniques_only(hashables): pass")
    cProfile.run("for _ in uniques_only(unhashables): pass")
