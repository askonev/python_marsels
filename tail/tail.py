from collections import deque


def tail(data, count_last: int):
	if count_last <= 0:
		return []
	return list(deque(data, count_last))


if __name__ == '__main__':
	print(tail([1, 2], 2))
	nums = (n**2 for n in [1, 2, 3, 4])
	print(tail(nums, -1), [])  # Don't loop for negative n
	print(tail(nums, 0), [])  # Don't loop for n=0
	print(tail(nums, 2), [9, 16])  # Consuming the generator
	print(list(nums), [])  # The nums generator is now empty
	print(tail(nums, 1), [])  # n=1 with a now empty generator
