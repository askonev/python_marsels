from typing import Iterable
from collections.abc import Iterable as AbcIterable


def _deep_flatten(iterable: Iterable) -> Iterable:
	levels = []

	levels.append(iter(iterable))
	while levels:
		try:
			item = next(levels[-1])
		except StopIteration:
			levels.pop()
			continue

		try:
			if isinstance(item, str):
				raise TypeError
			item = iter(item)
		except TypeError:
			yield item
		else:
			levels.append(item)


def deep_flatten(iterable: Iterable) -> Iterable:
	END_OF_ITER = object()
	levels = [iter(iterable)]

	while levels:
		item = next(levels[-1], END_OF_ITER)
		if item == END_OF_ITER:
			levels.pop()
			continue

		if isinstance(item, AbcIterable)\
			and not isinstance(item, (str, bytes)):
			levels.append(iter(item))
		else:
			yield item


if __name__ == '__main__':
	inputs = [
		['cats', ['carl', 'cate']],
		['dogs', ['darlene', 'doug']],
	]
	# inputs = [0, [1, [2, 3]], [4]]
	print(list(deep_flatten(inputs)))
