from contextlib import contextmanager
from sys import exc_info
from types import TracebackType
from typing import Optional, Iterable
import dataclass


@dataclass
class ExceptionInfo:
	exception: Optional[Exception] = None
	traceback: Optional[TracebackType] = None


@contextmanager
def suppress(*exception_types: type(Exception)) -> Iterable[ExceptionInfo]:
	context = ExceptionInfo()
	try:
		yield context
	except exception_types:
		_, v, t = exc_info()
		context.exception = v
		context.traceback = t
