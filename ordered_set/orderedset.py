from typing import Iterable
from itertools import zip_longest
from collections.abc import MutableSet


class OrderedSet(MutableSet):
	def __init__(self, data: Iterable = ()):
		self.items = dict.fromkeys(data, None)

		for item in data:
			if item not in self.items:
				self.items[item] = None

	def __str__(self):
		return str(self.items)

	def __iter__(self):
		return iter(self.items.keys())

	def __len__(self):
		return len(self.items)

	def add(self, item):
		self.items[item] = None

	def discard(self, item_to_remove):
		self.items.pop(item_to_remove, None)

	def __getitem__(self, index):
		_index = index
		if _index < 0:
			_index += len(self.items)
		for i, item in enumerate(self.items):
			if _index == i:
				return item
		raise IndexError

	def __eq__(self, other):
		if isinstance(other, OrderedSet):
			return (
				len(self) == len(other)
				and all((
					self_item == other_item
					for self_item, other_item in zip(self, other)))
			)
		else:
			return super().__eq__(other)

	def __contains__(self, item):
		return item in self.items


if __name__ == '__main__':
	orset = OrderedSet([1, 2, 3, 3])
	print(*orset)
	print(orset[0])
	print(OrderedSet('abc') == set('abc'))
