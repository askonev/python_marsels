from collections.abc import Iterable, Callable


class ParserSM:
	STATUS_REPEAT: int = 0
	STATUS_NEXT: int = 1
	STATUS_FINISH: int = 2
	EOL = '\0'

	def __init__(self):
		self.handle_state: Callable = self.state_range_start
		self.number_digits: list = []
		self.start: int = 0
		self.stop: int = 0
		self.text_iter: Iterable = iter("")
		self.from_to_sign = '-'
		self.delimiter = ','
		self.ready: bool = False
		self.result: Iterable = range(0, 0)

	def run(self, sequence: str, delimiter: str = ',', from_to_sign: str = '-'):
		self.delimiter = delimiter
		self.from_to_sign = from_to_sign

		status = ParserSM.STATUS_NEXT
		text_iter = iter(sequence)
		symbol = next(text_iter, self.EOL)
		while status is not ParserSM.STATUS_FINISH:
			status = self.handle_state(symbol)
			symbol = next(text_iter, self.EOL) \
				if status is ParserSM.STATUS_NEXT \
				else symbol

			if self.ready:
				yield from self.result
				self.ready = False

	def state_range_start(self, symbol: str) -> int:
		if symbol.isdigit():
			self.number_digits.append(symbol)
			status = self.STATUS_NEXT
		else:
			self.start = int("".join(map(str, self.number_digits)))
			self.stop = self.start
			self.number_digits = []
			if symbol is self.from_to_sign:
				self.handle_state = self.state_fromto_sign
			elif symbol is self.EOL:
				self.handle_state = self.state_finish
			else:
				self.handle_state = self.state_range_splitter
			status = self.STATUS_REPEAT
		return status

	def state_fromto_sign(self, symbol: str):
		if symbol.isdigit():
			self.handle_state = self.state_range_end
			status = self.STATUS_REPEAT
		elif symbol is self.from_to_sign:
			status = self.STATUS_NEXT
		else:
			self.handle_state = self.state_coverage_data
			status = self.STATUS_REPEAT
		return status

	def state_range_end(self, symbol: str):
		if symbol.isdigit():
			self.number_digits.append(symbol)
			status = self.STATUS_NEXT
		else:
			self.stop = int("".join(map(str, self.number_digits)))
			self.number_digits = []
			if symbol is self.EOL:
				self.handle_state = self.state_finish
			else:
				self.handle_state = self.state_range_splitter
			status = self.STATUS_REPEAT
		return status

	def state_coverage_data(self, symbol: str):
		if symbol is not self.delimiter:
			status = self.STATUS_NEXT
		else:
			self.handle_state = self.state_range_splitter
			status = self.STATUS_REPEAT
		return status

	def state_range_splitter(self, symbol: str) -> int:
		self.ready = True
		self.result = range(self.start, self.stop + 1)
		self.handle_state = self.state_space_consumption
		return self.STATUS_NEXT

	def state_space_consumption(self, symbol: str) -> int:
		status = self.STATUS_NEXT
		if symbol.isdigit():
			self.handle_state = self.state_range_start
			status = self.STATUS_REPEAT
		return status

	def state_finish(self, symbol: str):
		self.ready = True
		self.result = range(self.start, self.stop + 1)
		return self.STATUS_FINISH


def parse_ranges(ranges: str) -> Iterable:
	yield from ParserSM().run(ranges)


if __name__ == '__main__':
	print(list(parse_ranges('0-0, 4-8, 20-21, 43-45')))
