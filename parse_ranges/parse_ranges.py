from collections.abc import Iterable
from re import search


def parse_ranges(ranges: str) -> Iterable:
	for item in ranges.split(','):
		reitem = search(r"(\d+-\d+)|(\d+)", item)
		if not reitem:
			continue
		start, _, stop = reitem.group(0).partition('-')
		if stop:
			yield from range(int(start), int(stop) + 1)
		else:
			yield int(start)


if __name__ == '__main__':
	# print(list(parse_ranges('0, 4-8, 20->exit, 43-45')))
	print(list(parse_ranges('0, 4-8, 20->exit, 43-45')))
