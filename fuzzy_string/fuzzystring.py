from unicodedata import normalize
from typing import Union
from collections import UserString


class FuzzyString(UserString):
	def __eq__(self, other):
		return self.normalize(self) == self.normalize(other)

	def __contains__(self, other):
		return self.normalize(other) in self.normalize(self)

	def __add__(self, other):
		return FuzzyString(self.data + other)

	def __lt__(self, other):
		return self.normalize(self) < self.normalize(other)

	def __le__(self, other):
		return self < other or self == other

	def __gt__(self, other):
		return not self <= other

	def __ge__(self, other):
		return not self < other

	def __len__(self):
		return len(self.normalize(self.data))

	@staticmethod
	def normalize(text: Union[str, 'FuzzyString']):
		if isinstance(text, FuzzyString):
			target = text.data
		else:
			target = text
		return normalize("NFKD", target.casefold())


import unittest


class FuzzyStringTests(unittest.TestCase):
	def test_other_string_comparisons(self):
		# apple = FuzzyString("Apple")
		# self.assertGreater(apple, "animal")

		new_delhi = FuzzyString("NeW DELhi")
		new = FuzzyString("New")
		delhi = FuzzyString("Delhi")
		self.assertEqual(new + " " + delhi, new_delhi)


if __name__ == '__main__':
	new = FuzzyString("New")
	delhi = FuzzyString("Delhi")
	print(type(" " + delhi))
	unittest.main(verbosity=2)
