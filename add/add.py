
def get_shape(matrix):
    return [len(row) for row in matrix]

def add(*matrices):
    shape = get_shape(matrices[0])
    if any(shape != get_shape(m) for m in matrices):
        raise ValueError

    return [[sum(columns) for columns in zip(*rows)]
            for rows in zip(*matrices)]
