from configparser import ConfigParser
from os import path
from argparse import ArgumentParser
from argparse import ArgumentDefaultsHelpFormatter
from typing import Iterable
import pathlib
import csv


def parse_ini(file: path) -> ConfigParser:
	parser = ConfigParser()
	parser.read(file)
	yield from (
		(section, name, parser[section][name])
		for section in parser.sections()
		for name in parser[section]
		if section != 'DEFAULT')


def parse_ini_flattened(file: path) -> ConfigParser:
	parser = ConfigParser()
	parser.read(file)

	#  Scan for names
	yield 'header', *(name for name in parser[parser.sections()[0]])

	for section in parser.sections():
		line = list()
		line.append(section)
		for name in parser[section]:
			line.append(parser[section][name])
		yield line


def save_csv(data: Iterable, csv_file: str, delimiter: str = ','):
	with open(csv_file, 'w', newline='') as file:
		writer = csv.writer(file, delimiter=delimiter)
		writer.writerows(data)


if __name__ == '__main__':
	argparser = ArgumentParser(
		description="Converts INI to CSV",
		formatter_class=ArgumentDefaultsHelpFormatter)

	argparser.add_argument(
		'ini_file',
		type=pathlib.Path,
		default='config.ini',
		help='Path to INI-file')

	argparser.add_argument(
		'output_csv_file',
		type=pathlib.Path,
		default='config.csv',
		help='Path to the resulting file')

	argparser.add_argument(
		'--collapsed',
		action='store_true',
		help='Resulting CSV-file will be collapsed')

	parsed_args = argparser.parse_args()

	ini_path = parsed_args.ini_file
	if parsed_args.collapsed:
		cfg = parse_ini_flattened(ini_path)
	else:
		cfg = parse_ini(ini_path)
	save_csv(cfg, parsed_args.output_csv_file)