from itertools import zip_longest
from unicodedata import normalize


def is_anagram(first: str, second: str):
	def prepare(text: str):
		return sorted(filter(
			lambda c: c.isalpha(),
			normalize("NFD", text.lower())))

	for cd, cs in zip_longest(prepare(first), prepare(second)):
		if cd != cs:
			return False
	return True


if __name__ == '__main__':
	print(is_anagram("Siobhán Donaghy", "Shanghai Nobody"))
