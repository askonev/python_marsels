

class alias:
    NO_ATTR = object()
    def __init__(self, attr_name, *, write=False):
        self.attr_name = attr_name
        self.write_en = write

    def __get__(self, obj, owner=None):
        if obj:
            return getattr(obj, self.attr_name)
        elif owner:
            return getattr(owner, self.attr_name)
        else:
            raise AttributeError

    def __set__(self, obj, value):
        if self.write_en:
            setattr(obj, self.attr_name, value)
        else:
            raise AttributeError


if __name__ == "__main__":

    
    class Thing:
        one = 4
        two = alias('one')
    assert Thing.one == 4
    assert Thing.two == 4



