from itertools import zip_longest
from itertools import chain

def interleave_v1(*args):
    liter = []
    working = True
    for item in args:
        liter.append(iter(item))
    while(working):
        working = False
        for item in liter:
            try:
                value = next(item)
                yield value
            except StopIteration:
                pass
            else:
                working = working or True


def interleave_v2(*args):
    EMPTY = object()
    for item in zip_longest(*args, fillvalue=EMPTY):
       yield from (
           value
           for value in item
           if value is not EMPTY) 

def interleave(*args):
    iterators = [iter(item) for item in args]
    while iterators:
        for iter_item in iterators:
            try:
                yield next(iter_item)
            except StopIteration:
                iterators.remove(iter_item)




if __name__ == "__main__":
#    print([item for item in interleave([1, 2, 3], [4, 5], [6, 7, 8, 9])])

    in1 = [1, 2, 3]
    in2 = [4, 5, 6, 7, 8]
    in3 = [9]
    out1 = [1, 4, 9, 2, 5, 3, 6, 7, 8]
    print([item for item in interleave(in1, in2, in3)])

    #assert interleave(in1, in2, in3) == out1
    #assert (
    #    interleave([1, 2], [3], [4, 5, 6], [7, 8], [9])
    #    == [1, 3, 4, 7, 9, 2, 5, 8, 6],
    #)
    


