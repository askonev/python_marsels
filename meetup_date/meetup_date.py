from datetime import date
from enum import (IntEnum, auto)

class Weekday(IntEnum):
    MONDAY = 0
    TUESDAY   = auto()
    WEDNESDAY = auto()
    THURSDAY  = auto()
    FRIDAY    = auto()
    SATURDAY  = auto()
    SUNDAY    = auto()


def meetup_date(
    year: int,
    month: int,
    nth: int = 4,
    weekday: Weekday = Weekday.THURSDAY) -> date:
    DAYS_PER_WEEK = 7
    WEEKS_PER_MONTH = 5
    nth_ = ((WEEKS_PER_MONTH + 1) + nth) % (WEEKS_PER_MONTH + 1)
    month_first_weekday = date(year, month, 1).weekday() 
    days_to_first_input_day = (weekday + 7 - month_first_weekday) % 7

    res = days_to_first_input_day + DAYS_PER_WEEK * (nth_ - 1) + 1
    res = res if res <= date.max.day else res - DAYS_PER_WEEK
    return date(year, month, res)


if __name__ == "__main__":
    print(meetup_date(2022, 11))
    print(meetup_date(2015, 8))
    print(meetup_date(2016, 2, nth=4, weekday=3))
    print(meetup_date(2018, 1, nth=1, weekday=0))
    print(meetup_date(2018, 1, nth=5, weekday=Weekday.WEDNESDAY))
    print(meetup_date(2018, 1, nth=-1, weekday=Weekday.FRIDAY))
    print(meetup_date(2018, 6, nth=4, weekday=Weekday.SUNDAY))
    print(meetup_date(2022, 2, nth=100203, weekday=Weekday.MONDAY))

