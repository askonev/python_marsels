from dataclasses import dataclass, astuple
from numbers import Number

@dataclass(eq=True, frozen=True, init=True)
class Vector:
    x: int
    y: int
    z: int

    __slots__ = 'x', 'y', 'z'

    def __add__(self, other):
        if not isinstance(other, Vector):
            return NotImplemented
        return Vector(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z)

    def __sub__(self, other):
        if not isinstance(other, Vector):
            return NotImplemented
        return Vector(
            self.x - other.x,
            self.y - other.y,
            self.z - other.z)

    def __mul__(self, number: Number):
        if not isinstance(number, Number):
            return NotImplemented
        return Vector(
            self.x * number,
            self.y * number,
            self.z * number)

    __rmul__ = __mul__

    def __truediv__(self, number: Number):
        if not isinstance(number, Number):
            return NotImplemented
        return Vector(
            self.x / number,
            self.y / number,
            self.z / number)

    def __iter__(self):
        yield from astuple(self)


if __name__ == "__main__":
    v = Vector(1, 2, 3)

    print(v * 3)
    print(3 * v)
    print(v / 2)
