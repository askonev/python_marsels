from functools import wraps
from dataclasses import dataclass


NO_RETURN = object()


@dataclass
class Calls_track:
    args: list
    kwargs: dict
    return_value: object = NO_RETURN
    exception: Exception = None


def record_calls(func):

    @wraps(func)
    def wrapper(*args, **kwargs):
        print(f'added func={func.__name__}')
        wrapper.call_count += 1
        call_track = Calls_track(args=args, kwargs=kwargs)
        wrapper.calls.append(call_track)
        try:
            call_track.return_value = func(*args, **kwargs)
        except Exception as e:
            call_track.exception = e
            call_track.return_value = NO_RETURN
            raise
        else:
            call_track.exception = None
        return call_track.return_value

    wrapper.call_count = 0
    wrapper.calls = []

    return wrapper
        

if __name__ == "__main__":
    
    @record_calls
    def greet(name="world"):
        """Greet someone by their name."""
        print(f"Hello {name}")

    greet("Trey")
    print(greet.call_count)

    @record_calls
    def my_func(*args, **kwargs): return sum(args), kwargs
    my_func()
    assert my_func.calls[0].return_value == (0, {})
    my_func(1, 2, a=3)
    assert my_func.calls[1].return_value, (3, {'a': 3})
    assert my_func.calls[1].exception == None

    @record_calls
    def my_func3(*args, **kwargs): raise TypeError
    try:
        my_func3()
    except Exception:
        pass
    assert my_func3.calls[-1].return_value == NO_RETURN
    assert my_func3.calls[-1].exception == TypeError

