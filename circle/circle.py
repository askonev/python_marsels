from math import pi


class Circle(object):
    """Circle object"""

    def __init__(self, radius=1):
        super(Circle, self).__init__()
        self.radius = radius

    @property
    def diameter(self):
        return self._radius * 2

    @property
    def area(self):
        return pi * self._radius ** 2

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, radius):
        if radius < 0:
            raise ValueError("radius cannot be negative")
        self._radius = radius

    @diameter.setter
    def diameter(self, diameter):
        self.radius = diameter / 2

    @area.setter
    def area(self, area):
        raise AttributeError("Area connot be set")

    def __repr__(self):
        return f"Circle({self._radius})"


def main():
    c = Circle(5)


if __name__ == '__main__':
    main()
